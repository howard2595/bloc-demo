import 'package:bloc_test/bloc/register_bloc.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

import 'page/register_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Bloc Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: RegisterPage(bloc: RegisterBloc()),
      builder: (context, child) {
        return OKToast(
          radius: 14,
          position:
              const ToastPosition(align: Alignment.bottomCenter, offset: -140),
          textPadding: const EdgeInsets.fromLTRB(25, 18, 25, 18),
          textStyle: Theme.of(context).textTheme.bodyText2!.copyWith(
              fontSize: 14, color: Colors.white, fontWeight: FontWeight.w500),
          backgroundColor: Colors.black.withOpacity(0.85),
          child: MediaQuery(
            child: child!,
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          ),
        );
      },
    );
  }
}
