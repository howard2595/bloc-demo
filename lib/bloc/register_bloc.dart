import 'dart:async';

import 'package:bloc_test/bloc/bloc_base.dart';
import 'package:bloc_test/event/register_event.dart';
import 'package:bloc_test/state/register_state.dart';

class RegisterBloc extends BlocBase {
  // 事件输入，触发业务逻辑
  final eventController = StreamController<RegisterEvent>.broadcast();

  // email状态
  final emailStateController = StreamController<RegisterEmailInputState>();

  Stream<RegisterEmailInputState> get emailState => emailStateController.stream;

  // password1状态
  final password1StateController =
      StreamController<RegisterPassword1InputState>();

  Stream<RegisterPassword1InputState> get password1State =>
      password1StateController.stream;

  // password2状态
  final password2StateController =
      StreamController<RegisterPassword2InputState>();

  Stream<RegisterPassword2InputState> get password2State =>
      password2StateController.stream;

  // 注册按钮状态
  final registerBtnStateController = StreamController<RegisterBtnState>();

  Stream<RegisterBtnState> get registerBtnState =>
      registerBtnStateController.stream;

  RegisterBloc() {
    // 监听事件，触发具体业务逻辑
    eventController.stream.listen((event) async {
      if (event is RegisterEventEmailCheck) {
        // 校验邮箱
        _handleEmailCheck(event);
      } else if (event is RegisterEventPasswordLengthCheck) {
        // 密码长度校验
        _handlePasswordLengthCheck(event);
      } else if (event is RegisterEventPasswordFocusChange) {
        // 密码焦点发生变化
        _handlePasswordFocusChange(event);
      } else if (event is RegisterEventPasswordCheck) {
        // 密码校验
        _handlePasswordCheck(event);
      } else if (event is RegisterEventRegisterBtnEnable) {
        // 注册按钮是否可用
        _handleRegisterBtnEnable(event);
      } else if (event is RegisterEventRegister) {
        // 注册
        _handleRegister(event);
      }
    });
  }

  _handleEmailCheck(RegisterEventEmailCheck event) {
    registerBtnStateController.sink.add(RegisterBtnState.initialize());
    if (event.email.isEmpty) {
      emailStateController.sink.add(RegisterEmailInputState.initialize());
      return;
    }
    // 1.检测邮箱是否可用
    if (event.availableCheck) {
      if (!_emailAvailableCheck(event.email)) {
        emailStateController.sink.add(
            RegisterEmailInputState.emailCheckFailed('Invalid email address'));
      } else {
        // 检查成功，触发注册按钮可用
        eventController.sink.add(RegisterEventCheckBtnEnable());
      }
      return;
    }
    // 2.检查邮箱是否有非法字符
    if (!_emailCheckIllegalCharacter(event.email)) {
      emailStateController.sink.add(RegisterEmailInputState.emailCheckFailed(
          'Only letters(a-z), numbers(0-9), period(.), hyphen(-) and underscore(_) are allowed'));
    } else {
      emailStateController.sink
          .add(RegisterEmailInputState.emailCheckSuccess());
    }
  }

  bool _emailAvailableCheck(String email) {
    email = email.toLowerCase();
    RegExp _exp = RegExp('^[A-Za-z0-9-._=+]+@[-a-z0-9._=+]+\\.\\w+\$');
    return _exp.hasMatch(email);
  }

  bool _emailCheckIllegalCharacter(String email) {
    RegExp _exp = RegExp("^([\\a-zA-Z0-9\\u4E00-\\u9FA5_\\-.@])+\$");
    return _exp.hasMatch(email);
  }

  _handlePasswordLengthCheck(RegisterEventPasswordLengthCheck event) {
    int _length = event.password.length;
    String _helperText = '';
    String _counterText = '';
    if (_length < 6) {
      _helperText = 'At least 6 characters';
    } else {
      _counterText = '$_length/16';
    }
    if (event.isFirst) {
      password1StateController.sink.add(
          RegisterPassword1InputState.passwordInputLengthChange(
              _helperText, _counterText));
    } else {
      password2StateController.sink.add(
          RegisterPassword2InputState.passwordInputChange(
              _helperText, _counterText, ''));
    }
  }

  _handlePasswordFocusChange(RegisterEventPasswordFocusChange event) {
    // 获取到焦点
    if (event.hasFocus) {
      // 触发长度变化
      _handlePasswordLengthCheck(
          RegisterEventPasswordLengthCheck(event.isFirst, event.password));
      return;
    }
    // 失去焦点
    // 1.先移除相关提示信息
    if (event.isFirst) {
      password1StateController.sink
          .add(RegisterPassword1InputState.initialize());
    } else {
      password2StateController.sink
          .add(RegisterPassword2InputState.initialize());
    }
    // 2.如果是确认密码输入框，则触发校验两次密码是否一致
    if (!event.isFirst && event.password.isNotEmpty) {
      eventController.sink.add(RegisterEventCheckPassword());
    }
  }

  _handlePasswordCheck(RegisterEventPasswordCheck event) {
    if (event.password1 == event.password2) {
      // 两次密码一致，触发注册按钮可用
      eventController.sink.add(RegisterEventCheckBtnEnable());
    } else {
      // 两次密码不一致
      password2StateController.sink.add(
          RegisterPassword2InputState.passwordInputChange(
              '', '', 'The two passwords do not match'));
    }
  }

  _handleRegisterBtnEnable(RegisterEventRegisterBtnEnable event) {
    bool _enable =
        _emailAvailableCheck(event.email) && event.password1 == event.password2;
    registerBtnStateController.sink
        .add(RegisterBtnState.enableStateChange(_enable));
  }

  _handleRegister(RegisterEventRegister event) {
    // mock数据：已经存在的邮箱
    List<String> _mockExistEmail = [
      'inosuke@gmail.com',
      'inosuke1@gmail.com',
      'inosuke2@gmail.com',
      'inosuke3@gmail.com',
      'nezuko@gmail.com',
      'nezuko1@gmail.com',
      'nezuko2@gmail.com',
    ];
    // 1.先校验邮箱是否已经存在
    if (_mockExistEmail.contains(event.email)) {
      eventController.sink.add(RegisterEventToast('This Email Is Taken'));
      return;
    }
    // 2.校验邮箱是否合法
    if (!_emailAvailableCheck(event.email)) {
      eventController.sink.add(RegisterEventToast('Invalid email address'));
      return;
    }
    // 3.校验密码是否一致
    if (event.password1 != event.password2) {
      eventController.sink
          .add(RegisterEventToast('The two passwords do not match'));
      return;
    }
    // 4.注册成功跳转页面
    eventController.sink.add(RegisterEventSuccess(event.email));
  }

  /// 邮箱输入变更
  void onEmailInputChange(String email) {
    eventController.sink.add(RegisterEventEmailCheck(false, email));
  }

  /// 邮箱焦点变化
  void onEmailInputFocusChange(bool hasFocus, String email) {
    if (hasFocus) {
      // 获取焦点时，清除之前的状态
      emailStateController.sink.add(RegisterEmailInputState.initialize());
    } else {
      onEmailInputComplete(email);
    }
  }

  /// 邮箱输入完成
  void onEmailInputComplete(String email) {
    // 失去焦点时，校验邮箱是否合法
    eventController.sink.add(RegisterEventEmailCheck(true, email));
  }

  /// 密码输入变更
  void onPasswordInputChange(bool isFirst, String password) {
    eventController.sink
        .add(RegisterEventPasswordLengthCheck(isFirst, password));
  }

  /// 密码输入焦点变化
  void onPasswordInputFocusChange(
      bool hasFocus, bool isFirst, String password) {
    eventController.sink
        .add(RegisterEventPasswordFocusChange(hasFocus, isFirst, password));
  }

  /// 当密码输入完成
  void onPasswordInputComplete(String password1, String password2) {
    eventController.sink.add(RegisterEventPasswordCheck(password1, password2));
  }

  /// 注册按钮是否可用
  void checkRegisterBtnEnable(
      String email, String password1, String password2) {
    eventController.sink
        .add(RegisterEventRegisterBtnEnable(email, password1, password2));
  }

  /// 注册
  void register(String email, String password1, String password2) {
    eventController.sink
        .add(RegisterEventRegister(email, password1, password2));
  }

  @override
  void dispose() {
    eventController.close();
    emailStateController.close();
    password1StateController.close();
    password2StateController.close();
    registerBtnStateController.close();
  }
}
