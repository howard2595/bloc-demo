import 'package:bloc_test/bloc/register_bloc.dart';
import 'package:bloc_test/event/register_event.dart';
import 'package:bloc_test/page/register_success_page.dart';
import 'package:bloc_test/state/register_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oktoast/oktoast.dart';

class RegisterPage extends StatefulWidget {
  final RegisterBloc bloc;

  const RegisterPage({Key? key, required this.bloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late TextEditingController _emailInputController;
  late FocusNode _emailInputFocusNode;
  late TextEditingController _password1InputController;
  late FocusNode _password1InputFocusNode;
  late TextEditingController _password2InputController;
  late FocusNode _password2InputFocusNode;

  @override
  void initState() {
    super.initState();
    widget.bloc.eventController.stream.listen((event) {
      // 弹Toast
      if (event is RegisterEventToast) {
        showToast(event.msg);
      }
      // 注册成功跳转页面
      else if (event is RegisterEventSuccess) {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (_) => RegisterSuccessPage(email: event.email)));
      }
      // 校验密码
      else if (event is RegisterEventCheckPassword) {
        widget.bloc.onPasswordInputComplete(
            _password1InputController.text, _password2InputController.text);
      }
      // 检查按钮是否可用
      else if (event is RegisterEventCheckBtnEnable) {
        widget.bloc.checkRegisterBtnEnable(_emailInputController.text,
            _password1InputController.text, _password2InputController.text);
      }
    });
    _emailInputController = TextEditingController();
    _emailInputFocusNode = FocusNode();
    _password1InputController = TextEditingController();
    _password1InputFocusNode = FocusNode();
    _password2InputController = TextEditingController();
    _password2InputFocusNode = FocusNode();
    // 添加监听
    _emailInputFocusNode.addListener(() => widget.bloc.onEmailInputFocusChange(
        _emailInputFocusNode.hasFocus, _emailInputController.text));
    _password1InputFocusNode.addListener(() => widget.bloc
        .onPasswordInputFocusChange(_password1InputFocusNode.hasFocus, true,
            _password1InputController.text));
    _password2InputFocusNode.addListener(() => widget.bloc
        .onPasswordInputFocusChange(_password2InputFocusNode.hasFocus, false,
            _password2InputController.text));
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    _emailInputController.dispose();
    _emailInputFocusNode.dispose();
    _password1InputController.dispose();
    _password1InputFocusNode.dispose();
    _password2InputController.dispose();
    _password2InputFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text('Register Page'),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            padding: const EdgeInsets.only(
                left: 20, right: 20, top: 100, bottom: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'Create Account',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueAccent,
                  ),
                ),
                const SizedBox(height: 20),
                // 邮箱输入框
                _buildEmailInput(),
                const SizedBox(height: 15),
                // 密码输入框
                StreamBuilder(
                  stream: widget.bloc.password1State,
                  initialData: RegisterPassword1InputState.initialize(),
                  builder:
                      (_, AsyncSnapshot<RegisterPassword1InputState> snapshot) {
                    String _helperText = snapshot.data?.helperText ?? '';
                    String _counterText = snapshot.data?.counterText ?? '';
                    return _buildPasswordInput(
                        helperText: _helperText, counterText: _counterText);
                  },
                ),
                const SizedBox(height: 15),
                // 密码确认输入框
                StreamBuilder(
                  stream: widget.bloc.password2State,
                  initialData: RegisterPassword2InputState.initialize(),
                  builder:
                      (_, AsyncSnapshot<RegisterPassword2InputState> snapshot) {
                    String _helperText = snapshot.data?.helperText ?? '';
                    String _counterText = snapshot.data?.counterText ?? '';
                    String _errorText = snapshot.data?.errorText ?? '';
                    return _buildPasswordInput(
                        isFirst: false,
                        helperText: _helperText,
                        counterText: _counterText,
                        errorText: _errorText);
                  },
                ),
                const SizedBox(height: 20),
                // 注册按钮
                _buildRegisterBtn(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildEmailInput() {
    return StreamBuilder(
      stream: widget.bloc.emailState,
      initialData: RegisterEmailInputState.initialize(),
      builder: (_, AsyncSnapshot<RegisterEmailInputState> snapshot) {
        bool _showError = snapshot.data?.emailCheckApprove ?? false;
        String _errorMsg = snapshot.data?.emailCheckFailedMsg ?? '';

        return TextFormField(
          controller: _emailInputController,
          focusNode: _emailInputFocusNode,
          decoration: InputDecoration(
            labelText: 'Email',
            hintText: 'Enter Your Email',
            errorMaxLines: 3,
            errorText: _errorMsg.isEmpty ? null : _errorMsg,
            border: const OutlineInputBorder(
              gapPadding: 10,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            prefixIcon: const Icon(Icons.email_rounded),
          ),
          onChanged: (email) =>
              widget.bloc.onEmailInputChange(_emailInputController.text),
          onEditingComplete: () {
            _emailInputFocusNode.unfocus();
            _password1InputFocusNode.requestFocus();
            widget.bloc.onEmailInputComplete(_emailInputController.text);
          },
          keyboardType: TextInputType.emailAddress,
        );
      },
    );
  }

  _buildPasswordInput(
      {bool isFirst = true,
      String helperText = '',
      String counterText = '',
      String errorText = ''}) {
    return TextFormField(
      controller:
          isFirst ? _password1InputController : _password2InputController,
      focusNode: isFirst ? _password1InputFocusNode : _password2InputFocusNode,
      decoration: InputDecoration(
        labelText: isFirst ? 'Password' : 'Confirm Password',
        hintText: '${isFirst ? 'Enter' : 'Confirm'} Your Password',
        helperMaxLines: 3,
        helperText: helperText.isEmpty ? null : helperText,
        counterText: counterText.isEmpty ? null : counterText,
        errorText: errorText.isEmpty ? null : errorText,
        border: const OutlineInputBorder(
          gapPadding: 10,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        prefixIcon: const Icon(Icons.password_rounded),
      ),
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(16),
      ],
      onChanged: (password) =>
          widget.bloc.onPasswordInputChange(isFirst, password),
      onEditingComplete: () {
        // 密码输入框
        if (isFirst) {
          _password1InputFocusNode.unfocus();
          _password2InputFocusNode.requestFocus();
          return;
        }
        // 确认密码输入框
        _password2InputFocusNode.unfocus();
      },
    );
  }

  _buildRegisterBtn() {
    return StreamBuilder(
      stream: widget.bloc.registerBtnState,
      initialData: RegisterBtnState.initialize(),
      builder: (_, AsyncSnapshot<RegisterBtnState> snapshot) {
        bool _enable = snapshot.data?.registerBtnEnable ?? false;
        return GestureDetector(
          onTap: _enable
              ? () {
                  widget.bloc.register(
                      _emailInputController.text,
                      _password1InputController.text,
                      _password2InputController.text);
                }
              : null,
          child: Container(
            width: 200,
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(25)),
              color: _enable ? Colors.blueAccent : Colors.grey,
            ),
            child: const Text(
              'Register',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      },
    );
  }
}
