import 'package:flutter/material.dart';

class RegisterSuccessPage extends StatefulWidget {
  final String? email;

  const RegisterSuccessPage({Key? key, this.email}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RegisterSuccessPageState();
}

class _RegisterSuccessPageState extends State<RegisterSuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register Success Page'),
      ),
      body: Center(
        child: Text('${widget.email}, welcome!'),
      ),
    );
  }
}
