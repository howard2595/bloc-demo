abstract class RegisterEvent {}

/// 邮箱校验事件
class RegisterEventEmailCheck extends RegisterEvent {
  final bool availableCheck;
  final String email;

  RegisterEventEmailCheck(this.availableCheck ,this.email);
}

/// 密码长度校验事件
class RegisterEventPasswordLengthCheck extends RegisterEvent {
  final bool isFirst;
  final String password;

  RegisterEventPasswordLengthCheck(this.isFirst, this.password);
}

/// 密码校验事件
class RegisterEventPasswordCheck extends RegisterEvent {
  final String password1;
  final String password2;

  RegisterEventPasswordCheck(this.password1, this.password2);
}

/// 密码焦点变化事件
class RegisterEventPasswordFocusChange extends RegisterEvent {
  final bool hasFocus;
  final bool isFirst;
  final String password;

  RegisterEventPasswordFocusChange(this.hasFocus, this.isFirst, this.password);
}

/// 注册事件
class RegisterEventRegister extends RegisterEvent {
  final String email;
  final String password1;
  final String password2;

  RegisterEventRegister(this.email, this.password1, this.password2);
}

/// 注册按钮是否可用事件
class RegisterEventRegisterBtnEnable extends RegisterEvent {
  final String email;
  final String password1;
  final String password2;

  RegisterEventRegisterBtnEnable(this.email, this.password1, this.password2);
}

/// 展示Toast事件
class RegisterEventToast extends RegisterEvent {
  final String msg;

  RegisterEventToast(this.msg);
}

/// 注册成功事件
class RegisterEventSuccess extends RegisterEvent {
  final String email;

  RegisterEventSuccess(this.email);
}

/// 校验密码事件
class RegisterEventCheckPassword extends RegisterEvent {}

/// 检查Btn是否可用事件
class RegisterEventCheckBtnEnable extends RegisterEvent {}
