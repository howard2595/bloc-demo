/// 注册邮箱输入状态
class RegisterEmailInputState {
  final bool emailCheckApprove;
  final String emailCheckFailedMsg;

  RegisterEmailInputState({
    this.emailCheckApprove = true,
    this.emailCheckFailedMsg = '',
  });

  /// 初始状态
  factory RegisterEmailInputState.initialize() {
    return RegisterEmailInputState();
  }

  /// 邮箱校验失败
  factory RegisterEmailInputState.emailCheckFailed(String msg) {
    return RegisterEmailInputState(
      emailCheckApprove: false,
      emailCheckFailedMsg: msg,
    );
  }

  /// 邮箱校验成功
  factory RegisterEmailInputState.emailCheckSuccess() {
    return RegisterEmailInputState();
  }
}

/// 注册密码输入状态
class RegisterPassword1InputState {
  final String helperText;
  final String counterText;

  RegisterPassword1InputState({
    this.helperText = '',
    this.counterText = '',
  });

  /// 初始状态
  factory RegisterPassword1InputState.initialize() {
    return RegisterPassword1InputState();
  }

  /// 密码输入长度变化
  factory RegisterPassword1InputState.passwordInputLengthChange(
      String helperText, String counterText) {
    return RegisterPassword1InputState(
      helperText: helperText,
      counterText: counterText,
    );
  }
}

/// 注册密码输入状态
class RegisterPassword2InputState {
  final String helperText;
  final String counterText;
  final String errorText;

  RegisterPassword2InputState({
    this.helperText = '',
    this.counterText = '',
    this.errorText = '',
  });

  /// 初始状态
  factory RegisterPassword2InputState.initialize() {
    return RegisterPassword2InputState();
  }

  /// 密码输入变化
  factory RegisterPassword2InputState.passwordInputChange(
      String helperText, String counterText, String errorText) {
    return RegisterPassword2InputState(
      helperText: helperText,
      counterText: counterText,
      errorText: errorText,
    );
  }
}

/// 注册按钮状态
class RegisterBtnState {
  final bool registerBtnEnable;

  RegisterBtnState({
    this.registerBtnEnable = false,
  });

  /// 初始状态
  factory RegisterBtnState.initialize() {
    return RegisterBtnState();
  }

  /// 密码输入长度变化
  factory RegisterBtnState.enableStateChange(bool enable) {
    return RegisterBtnState(
      registerBtnEnable: enable,
    );
  }
}
